import no.noroff.accelerate.PrimaryAttribute;
import no.noroff.accelerate.character.Mage;
import no.noroff.accelerate.character.Warrior;
import no.noroff.accelerate.equipment.MageEquipment;
import no.noroff.accelerate.equipment.WarriorEquipment;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.item.*;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ItemTests {

    @Test
    void equipWarrior_invalidWeaponLevel_shouldThrowInvalidWeaponException() {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Weapon testWeapon = new Weapon("Great axe", 2, WeaponType.AXE);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        assertThrows(InvalidWeaponException.class, () -> warriorEquipment.equip(testWeapon));
    }

    @Test
    void equipWarrior_invalidArmorLevel_shouldThrowInvalidArmorException() {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Armor testArmor = new Armor("Common Body Plate Armor", 2);
        testArmor.setItemSlot(ItemSlot.BODY);
        testArmor.setArmorType(ArmorType.PLATE);
        testArmor.setArmorAttributes(new PrimaryAttribute(1, 0, 0));

        assertThrows(InvalidArmorException.class, () -> warriorEquipment.equip(testArmor));
    }

    @Test
    void equipWarrior_invalidWeaponType_shouldThrowInvalidWeaponException() {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Weapon testWeapon = new Weapon("Bow of Speed", 1, WeaponType.BOW);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(3);
        testWeapon.setAttackSpeed(2.1);

        assertThrows(InvalidWeaponException.class, () -> warriorEquipment.equip(testWeapon));
    }

    @Test
    void equipWarrior_invalidArmorType_shouldThrowInvalidArmorException() {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Armor testArmor = new Armor("Leather Armor", 1);
        testArmor.setItemSlot(ItemSlot.BODY);
        testArmor.setArmorType(ArmorType.LEATHER);
        testArmor.setArmorAttributes(new PrimaryAttribute(0, 1, 0));

        assertThrows(InvalidArmorException.class, () -> warriorEquipment.equip(testArmor));
    }

    @Test
    void equipWarrior_validWeapon_shouldReturnTrue() throws InvalidWeaponException {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Weapon testWeapon = new Weapon("Axe of Strength", 1, WeaponType.AXE);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(5);
        testWeapon.setAttackSpeed(1.3);

        boolean shouldReturnBool = warriorEquipment.equip(testWeapon);
        boolean expectedBool = true;

        assertEquals(expectedBool, shouldReturnBool);
    }

    @Test
    void equipWarrior_validArmorPiece_shouldReturnTrue() throws InvalidArmorException {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Armor testArmor = new Armor("Plate Mail Armor", 1);
        testArmor.setItemSlot(ItemSlot.BODY);
        testArmor.setArmorType(ArmorType.PLATE);
        testArmor.setArmorAttributes(new PrimaryAttribute(1, 0, 0));

        boolean shouldReturnBool = warriorEquipment.equip(testArmor);
        boolean expectedBool = true;

        assertEquals(expectedBool, shouldReturnBool);
    }

    @Test
    void checkDPSWarrior_unarmed_shouldReturnDouble() {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        myWarrior.updateTotalAttributes(warriorEquipment);
        myWarrior.calculateDPS(warriorEquipment);

        double actualDPS = myWarrior.getCharDPS();
        double expectedDPS = 1 * (1 + (5.0 / 100));

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void checkDPSWarrior_validWeapon_shouldReturnDouble() throws InvalidWeaponException {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Weapon testWeapon = new Weapon("Axe", 1, WeaponType.AXE);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        warriorEquipment.equip(testWeapon);

        myWarrior.updateTotalAttributes(warriorEquipment);
        myWarrior.calculateDPS(warriorEquipment);

        double actualDPS = myWarrior.getCharDPS();
        double expectedDPS = (7 * 1.1) * (1 + (5.0 / 100));

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void checkDPSWarrior_validWeaponAndArmor_shouldReturnDouble() throws InvalidArmorException, InvalidWeaponException {
        Warrior myWarrior = new Warrior("Conan the Barbarian");
        WarriorEquipment warriorEquipment = new WarriorEquipment(myWarrior);

        Weapon testWeapon = new Weapon("Axe", 1, WeaponType.AXE);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(7);
        testWeapon.setAttackSpeed(1.1);

        Armor testArmor = new Armor("Plate Body Armor", 1);
        testArmor.setArmorAttributes(new PrimaryAttribute(1, 0, 0));
        testArmor.setArmorType(ArmorType.PLATE);
        testArmor.setItemSlot(ItemSlot.BODY);

        warriorEquipment.equip(testWeapon);
        warriorEquipment.equip(testArmor);

        myWarrior.updateTotalAttributes(warriorEquipment);
        myWarrior.calculateDPS(warriorEquipment);

        double actualDPS = myWarrior.getCharDPS();
        double expectedDPS = (7 * 1.1) * (1 + ((5.0 + 1.0) / 100));

        assertEquals(expectedDPS, actualDPS);
    }

    @Test
    void checkDPSMage_validWeaponAndArmor_shouldReturnDouble() throws InvalidWeaponException, InvalidArmorException {
        Mage myMage = new Mage("Gandalf the Grey");
        MageEquipment mageEquipment = new MageEquipment(myMage);

        Weapon testWeapon = new Weapon("Staff of Fire", 1, WeaponType.STAFF);
        testWeapon.setItemSlot(ItemSlot.WEAPON);
        testWeapon.setDamage(5);
        testWeapon.setAttackSpeed(1.4);

        Armor testArmor = new Armor("Cloth Armor", 1);
        testArmor.setArmorAttributes(new PrimaryAttribute(0, 0, 1));
        testArmor.setArmorType(ArmorType.CLOTH);
        testArmor.setItemSlot(ItemSlot.BODY);

        mageEquipment.equip(testWeapon);
        mageEquipment.equip(testArmor);

        myMage.updateTotalAttributes(mageEquipment);
        myMage.calculateDPS(mageEquipment);

        double actualDPS = myMage.getCharDPS();
        double expectedDPS = (5 * 1.4) * (1 + ((8.0 + 1.0) / 100));

        assertEquals(expectedDPS, actualDPS);
    }

}

/*
1) If a character tries to equip a high level weapon, InvalidWeaponException should be thrown.
    o Use the warrior, and the axe, but set the axes level to 2.
2) If a character tries to equip a high level armor piece, InvalidArmorException should be thrown.
    o Use the warrior, and the plate body armor, but set the armor’s level to 2.
3) If a character tries to equip the wrong weapon type, InvalidWeaponException should be thrown.
    o Use the warrior and the bow.
4) If a character tries to equip the wrong armor type, InvalidArmorException should be thrown.
    o Use the warrior and the cloth armor.
5) If a character equips a valid weapon, a Boolean true should be returned.
6) If a character equips a valid armor piece, a Boolean true should be returned.
7) Calculate DPS if no weapon is equipped.
    o Take warrior at level 1
    o Expected DPS = 1*(1 + (5 / 100))
8) Calculate DPS with valid weapon equipped.
    o Take warrior level 1.
    o Equip axe.
    o Expected DPS = (7 * 1.1)*(1 + (5 / 100))
9) Calculate DPS with valid weapon and armor equipped.
    o Take warrior level 1.
    o Equip axe.
    o Equip plate body armor.
    o Expected DPS = (7 * 1.1) * (1 + ((5+1) / 100))
 */
