import no.noroff.accelerate.character.Mage;
import no.noroff.accelerate.character.Ranger;
import no.noroff.accelerate.character.Rogue;
import no.noroff.accelerate.character.Warrior;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CharacterTests {

    @Test
    void initialize_mage_shouldBeLevelOne () {
        Mage myMage = new Mage("Houdini");
        int shouldBeLevelOne = myMage.getLevel();
        int expectedLevel = 1;

        assertEquals(expectedLevel, shouldBeLevelOne);
    }

    @Test
    void levelUp_mage_shouldBeLevelTwo() {
        Mage myMage = new Mage("Houdini");
        myMage.levelUp();
        int shouldBeLevelTwo = myMage.getLevel();
        int expectedLevel = 1 + 1;

        assertEquals(expectedLevel, shouldBeLevelTwo);
    }

    @Test
    void get_mageBaseStats_shouldGetCorrectStats() {
        Mage myMage = new Mage("Houdini");
        int strength = myMage.getBaseStrength();
        int dexterity = myMage.getBaseDexterity();
        int intelligence = myMage.getBaseIntelligence();

        int expectedStrength = 1;
        int expectedDexterity = 1;
        int expectedIntelligence = 8;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void get_rangerBaseStats_shouldGetCorrectStats() {
        Ranger myRanger = new Ranger("Legolas");
        int strength = myRanger.getBaseStrength();
        int dexterity = myRanger.getBaseDexterity();
        int intelligence = myRanger.getBaseIntelligence();

        int expectedStrength = 1;
        int expectedDexterity = 7;
        int expectedIntelligence = 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void get_rogueBaseStats_shouldGetCorrectStats() {
        Rogue myRogue = new Rogue("Ezio");
        int strength = myRogue.getBaseStrength();
        int dexterity = myRogue.getBaseDexterity();
        int intelligence = myRogue.getBaseIntelligence();

        int expectedStrength = 2;
        int expectedDexterity = 6;
        int expectedIntelligence = 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void get_warriorBaseStats_shouldGetCorrectStats() {
        Warrior myWarrior = new Warrior("Kratos");
        int strength = myWarrior.getBaseStrength();
        int dexterity = myWarrior.getBaseDexterity();
        int intelligence = myWarrior.getBaseIntelligence();

        int expectedStrength = 5;
        int expectedDexterity = 2;
        int expectedIntelligence = 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void levelUp_mageBaseStats_shouldGetCorrectStats() {
        Mage myMage = new Mage("Houdini");
        myMage.levelUp();
        int strength = myMage.getBaseStrength();
        int dexterity = myMage.getBaseDexterity();
        int intelligence = myMage.getBaseIntelligence();

        int expectedStrength = 1 + 1;
        int expectedDexterity = 1 + 1;
        int expectedIntelligence = 8 + 5;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);

    }

    @Test
    void levelUp_rangerBaseStats_shouldGetCorrectStats() {
        Ranger myRanger = new Ranger("Legolas");
        myRanger.levelUp();
        int strength = myRanger.getBaseStrength();
        int dexterity = myRanger.getBaseDexterity();
        int intelligence = myRanger.getBaseIntelligence();

        int expectedStrength = 1 + 1;
        int expectedDexterity = 7 + 5;
        int expectedIntelligence = 1 + 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void levelUp_rogueBaseStats_shouldGetCorrectStats() {
        Rogue myRogue = new Rogue("Ezio");
        myRogue.levelUp();
        int intelligence = myRogue.getBaseIntelligence();
        int strength = myRogue.getBaseStrength();
        int dexterity = myRogue.getBaseDexterity();

        int expectedStrength = 2 + 1;
        int expectedDexterity = 6 + 4;
        int expectedIntelligence = 1 + 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }

    @Test
    void levelUp_warriorBaseStats_shouldGetCorrectStats() {
        Warrior myWarrior = new Warrior("Kratos");
        myWarrior.levelUp();
        int intelligence = myWarrior.getBaseIntelligence();
        int strength = myWarrior.getBaseStrength();
        int dexterity = myWarrior.getBaseDexterity();

        int expectedStrength = 5 + 3;
        int expectedDexterity = 2 + 2;
        int expectedIntelligence = 1 + 1;

        assertEquals(expectedStrength, strength);
        assertEquals(expectedDexterity, dexterity);
        assertEquals(expectedIntelligence, intelligence);
    }
}

/*
1) A character is level 1 when created.
2) When a character gains a level, it should be level 2.
3) Each character class is created with the proper default attributes.
    o Use level 1 stats for each character as expected.
    o This results in four test methods.
4) Each character class has their attributes increased when leveling up.
    o Create each class once, level them up once.
    o Use the base attributes, plus one instance of the level up as the expected.
    o E.g. Warrior -> levelup() -> ( Strength = 8, Dexterity = 4, Intelligence = 2) expected.
    o This results in four test methods.
 */
