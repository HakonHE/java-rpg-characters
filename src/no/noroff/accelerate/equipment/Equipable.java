package no.noroff.accelerate.equipment;

import no.noroff.accelerate.item.Armor;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.item.Weapon;

/**
 * An interface for functions for equipable equipment.
 */
public interface Equipable {
    boolean equip(Weapon weapon) throws InvalidWeaponException;

    boolean equip(Armor armor) throws InvalidArmorException;
}
