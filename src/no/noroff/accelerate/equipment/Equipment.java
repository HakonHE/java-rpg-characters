package no.noroff.accelerate.equipment;

import no.noroff.accelerate.PrimaryAttribute;
import no.noroff.accelerate.item.Armor;
import no.noroff.accelerate.item.ItemSlot;

import java.util.HashMap;

/**
 * A class for the equipment which implements the interface Equipable.
 */
public abstract class Equipment implements Equipable{
    protected HashMap<ItemSlot, Object> equipment = new HashMap<>();

    public HashMap<ItemSlot, Object> getEquipment() {
        return equipment;
    }

    /**
     * A getter for the armor attribute which first
     * gets the armor piece from the correct slot
     * and then returns the attributes of that armor.
     * @param slot the slot the armor piece occupies
     * @return PrimaryAttribute
     */
    public PrimaryAttribute getArmorAttribute(ItemSlot slot) {
        Armor armor = (Armor) equipment.get(slot);
        return armor.getArmorAttributes();
    }

    public Equipment() {
        equipment.put(ItemSlot.WEAPON, "");
        equipment.put(ItemSlot.HEAD, "");
        equipment.put(ItemSlot.BODY, "");
        equipment.put(ItemSlot.LEGS, "");
    }
}
