package no.noroff.accelerate.equipment;

import no.noroff.accelerate.character.Ranger;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.item.*;

/**
 * An equipment class for rangers.
 */
public class RangerEquipment extends Equipment {
    Ranger ranger;

    /**
     * Function for equipping a weapon to the character,
     * returns true if it succeeded.
     * @param weapon a weapon object e.g. battleaxe
     * @return boolean if the weapon is valid and the character can equip it return true
     * @throws InvalidWeaponException throws an exception if the weapon given is invalid
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (ranger.getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Not high enough level to equip this weapon.");
        }
        else if (weapon.getType() != WeaponType.BOW) {
            throw new InvalidWeaponException("Rangers can't use these types of weapons.");
        }
        else {
            equipment.replace(ItemSlot.WEAPON, weapon);
            return true;
        }
    }

    /**
     * Function for equipping an armor piece to the character,
     * returns true if it succeeded.
     * @param armor an armor object e.g. helmet head piece
     * @return boolean if the weapon is valid and the character can equip it return true
     * @throws InvalidArmorException throws exception if the armor piece given is invalid
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (ranger.getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Not high enough level to equip this armor.");
        }
        else if (armor.getArmorType() != ArmorType.LEATHER
                && armor.getArmorType() != ArmorType.MAIL) {
            throw new InvalidArmorException("Rangers can't use these types of armor.");
        }
        else {
            if (armor.getSlot() == ItemSlot.HEAD) {
                equipment.replace(ItemSlot.HEAD, armor);
            }
            else if (armor.getSlot() == ItemSlot.BODY) {
                equipment.replace(ItemSlot.BODY, armor);
            }
            else {
                equipment.replace(ItemSlot.LEGS, armor);
            }
            return true;
        }
    }

    public RangerEquipment(Ranger ranger) {
        this.ranger = ranger;
    }
}
