package no.noroff.accelerate.equipment;

import no.noroff.accelerate.character.Rogue;
import no.noroff.accelerate.exceptions.InvalidArmorException;
import no.noroff.accelerate.exceptions.InvalidWeaponException;
import no.noroff.accelerate.item.*;

/**
 * An equipment class for rogues.
 */
public class RogueEquipment extends Equipment {
    Rogue rogue;

    /**
     * Function for equipping a weapon to the character,
     * returns true if it succeeded.
     * @param weapon a weapon object e.g. battleaxe
     * @return boolean if the weapon is valid and the character can equip it return true
     * @throws InvalidWeaponException throws an exception if the weapon given is invalid
     */
    @Override
    public boolean equip(Weapon weapon) throws InvalidWeaponException {
        if (rogue.getLevel() < weapon.getRequiredLevel()) {
            throw new InvalidWeaponException("Not high enough level to equip this weapon.");
        }
        else if (weapon.getType() != WeaponType.DAGGER
                && weapon.getType() != WeaponType.SWORD) {
            throw new InvalidWeaponException("Rogues can't use these types of weapons.");
        }
        else {
            equipment.replace(ItemSlot.WEAPON, weapon);
            return true;
        }
    }

    /**
     * Function for equipping an armor piece to the character,
     * returns true if it succeeded.
     * @param armor an armor object e.g. helmet head piece
     * @return boolean if it was equipped return true
     * @throws InvalidArmorException throws exception if the armor piece given is invalid
     */
    @Override
    public boolean equip(Armor armor) throws InvalidArmorException {
        if (rogue.getLevel() < armor.getRequiredLevel()) {
            throw new InvalidArmorException("Not high enough level to equip this armor.");
        }
        else if (armor.getArmorType() != ArmorType.LEATHER
                && armor.getArmorType() != ArmorType.MAIL) {
            throw new InvalidArmorException("Rogues can't use these types of armor.");
        }
        else {
            if (armor.getSlot() == ItemSlot.HEAD) {
                equipment.replace(ItemSlot.HEAD, armor);
            }
            else if (armor.getSlot() == ItemSlot.BODY) {
                equipment.replace(ItemSlot.BODY, armor);
            }
            else {
                equipment.replace(ItemSlot.LEGS, armor);
            }
            return true;
        }
    }

    public RogueEquipment(Rogue rogue) {
        this.rogue = rogue;
    }
}
