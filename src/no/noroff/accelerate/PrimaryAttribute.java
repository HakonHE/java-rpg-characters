package no.noroff.accelerate;

public class PrimaryAttribute{
    private int strength;
    private int dexterity;
    private int intelligence;

    public PrimaryAttribute(int strength, int dexterity, int intelligence) {
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
    }

    public int getStrength() {
        return strength;
    }

    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setAttributes(int strength, int dexterity, int intelligence) {
        this.strength += strength;
        this.dexterity += dexterity;
        this.intelligence += intelligence;
    }

    /**
     * A setter for PrimaryAttributes which updates already existing values.
     * @param additional additional attributes to the base class specific attributes
     */
    public void updateAttributes(PrimaryAttribute additional) {
        strength += additional.strength;
        dexterity += additional.dexterity;
        intelligence += additional.intelligence;
    }

    /**
     * A setter for PrimaryAttributes which sets the values to base.
     * @param base the base value of the primary attributes for that class
     */
    public void setAttributes(PrimaryAttribute base) {
        strength = base.strength;
        dexterity = base.dexterity;
        intelligence = base.intelligence;
    }
}
