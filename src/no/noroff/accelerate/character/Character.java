package no.noroff.accelerate.character;

import no.noroff.accelerate.PrimaryAttribute;
import no.noroff.accelerate.equipment.Equipment;
import no.noroff.accelerate.item.ItemSlot;
import no.noroff.accelerate.item.Weapon;

/**
 * An abstract class for creating a character.
 */
public abstract class Character {
    private final String CharacterName;
    private int level;
    protected PrimaryAttribute base;
    protected PrimaryAttribute total;
    protected double charDPS = 0;

    /**
     * Initializes a character with a name and attributes from the class specialization.
     * @param name name for the character
     * @param strength starting strength of the character
     * @param dexterity starting dexterity of the character
     * @param intelligence starting intelligence of the character
     */
    public Character(String name, int strength, int dexterity, int intelligence) {
        CharacterName = name;
        level = 1;
        base = new PrimaryAttribute(strength, dexterity, intelligence);
        total = new PrimaryAttribute(strength, dexterity, intelligence);
    }

    public void setTotal(PrimaryAttribute base) {
        total.setAttributes(base);
    }

    public void updateTotal(PrimaryAttribute additional) {
        total.updateAttributes(additional);
    }

    public String getCharacterName() {
        return CharacterName;
    }

    public PrimaryAttribute getTotal() {
        return total;
    }

    public int getLevel() {
        return level;
    }

    public int getBaseStrength() {
        return base.getStrength();
    }

    public int getBaseDexterity() {
        return base.getDexterity();
    }

    public int getBaseIntelligence() {
        return base.getIntelligence();
    }

    public void levelUp(int strength, int dexterity, int intelligence) {
        level += 1;
        base.setAttributes(strength, dexterity, intelligence);
        total = base;
    }

    public double getCharDPS() {
        return charDPS;
    }

    /**
     * Takes the equipment of the character and then updates
     * total attribute with the attributes from the armor pieces.
     * @param equipment all the equipment of the character
     */
    public void updateTotalAttributes(Equipment equipment) {
        setTotal(base);
        if ( equipment.getEquipment().get(ItemSlot.HEAD) != "" ) {
            updateTotal(equipment.getArmorAttribute(ItemSlot.HEAD));
        }
        if ( equipment.getEquipment().get(ItemSlot.BODY) != "" ) {
            updateTotal(equipment.getArmorAttribute(ItemSlot.BODY));
        }
        if ( equipment.getEquipment().get(ItemSlot.LEGS) != "" ) {
            updateTotal(equipment.getArmorAttribute(ItemSlot.LEGS));
        }
    }

    /**
     * Calculates the dps of the character based on equipment
     * and total main attribute
     * @param totalMainAttribute a total of the class specific main attribute
     * @param equipment the equipment of the character
     */
    public void calculateDPS(int totalMainAttribute, Equipment equipment) {
        double weaponDPS = 1;
        if (equipment.getEquipment().get(ItemSlot.WEAPON) != "") {
            Weapon weapon = (Weapon) equipment.getEquipment().get(ItemSlot.WEAPON);
            weaponDPS = weapon.getDPS();
        }
        charDPS = weaponDPS * (1 + (totalMainAttribute  / 100.0));
    }
}
