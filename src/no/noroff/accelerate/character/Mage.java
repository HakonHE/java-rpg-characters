package no.noroff.accelerate.character;

import no.noroff.accelerate.equipment.MageEquipment;

/**
 * A child component of the character class for the mage class.
 */
public class Mage extends Character{

    private static final int BASE_STRENGTH = 1;
    private static final int BASE_DEXTERITY = 1;
    private static final int BASE_INTELLIGENCE = 8;
    private static final int LEVEL_STRENGTH = 1;
    private static final int LEVEL_DEXTERITY = 1;
    private static final int LEVEL_INTELLIGENCE = 5;

    public Mage(String name) {
        super(name, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE);

    }

    public void levelUp() {
        super.levelUp(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);
    }

    /**
     * Updates the total attributes for the character, adding in armor attributes.
     * @param equipment the mages equipment
     */
    public void updateTotalAttributes(MageEquipment equipment) {
        super.updateTotalAttributes(equipment);
    }

    /**
     * Calculates the mages dps.
     */
    public void calculateDPS(MageEquipment equipment) {
        super.calculateDPS(this.getTotal().getIntelligence(), equipment);
    }

}
