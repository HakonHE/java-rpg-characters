package no.noroff.accelerate.character;

import no.noroff.accelerate.equipment.RangerEquipment;

/**
 * A child component of the character class for the ranger class.
 */
public class Ranger extends Character{
    private static final int BASE_STRENGTH = 1;
    private static final int BASE_DEXTERITY = 7;
    private static final int BASE_INTELLIGENCE = 1;
    private static final int LEVEL_STRENGTH = 1;
    private static final int LEVEL_DEXTERITY = 5;
    private static final int LEVEL_INTELLIGENCE = 1;

    public Ranger(String name) {
        super(name, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE);

    }

    public void levelUp() {
        super.levelUp(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);
    }

    /**
     * Updates the total attributes for the character, adding in armor attributes.
     * @param equipment the rangers equipment
     */
    public void updateTotalAttributes(RangerEquipment equipment) { // MOVE TO CHARACTER AND CALL SUPER FROM EACH CLASS
        super.updateTotalAttributes(equipment);
    }

    /**
     * Calculates the rangers dps.
     */
    public void calculateDPS(RangerEquipment equipment) {
        super.calculateDPS(this.getTotal().getDexterity(), equipment);
    }

}
