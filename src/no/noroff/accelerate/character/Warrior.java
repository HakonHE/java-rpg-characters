package no.noroff.accelerate.character;

import no.noroff.accelerate.equipment.WarriorEquipment;

/**
 * A child component of the character class for the warrior class.
 */
public class Warrior extends Character{
    private static final int BASE_STRENGTH = 5;
    private static final int BASE_DEXTERITY = 2;
    private static final int BASE_INTELLIGENCE = 1;
    private static final int LEVEL_STRENGTH = 3;
    private static final int LEVEL_DEXTERITY = 2;
    private static final int LEVEL_INTELLIGENCE = 1;

    public Warrior(String name) {
        super(name, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE);

    }

    public void levelUp() {
        super.levelUp(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);
    }

    /**
     * Updates the total attributes for the character, adding in armor attributes.
     * @param equipment the warriors equipment
     */
    public void updateTotalAttributes(WarriorEquipment equipment) {
        super.updateTotalAttributes(equipment);
    }

    /**
     * Calculates the warriors dps.
     * @param equipment the warriors equipment
     */
    public void calculateDPS(WarriorEquipment equipment) {
        super.calculateDPS(this.getTotal().getStrength(), equipment);
    }
}
