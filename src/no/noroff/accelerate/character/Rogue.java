package no.noroff.accelerate.character;

import no.noroff.accelerate.equipment.RogueEquipment;

/**
 * A child component of the character class for the rogue class.
 */
public class Rogue extends Character{
    private static final int BASE_STRENGTH = 2;
    private static final int BASE_DEXTERITY = 6;
    private static final int BASE_INTELLIGENCE = 1;
    private static final int LEVEL_STRENGTH = 1;
    private static final int LEVEL_DEXTERITY = 4;
    private static final int LEVEL_INTELLIGENCE = 1;

    public Rogue(String name) {
        super(name, BASE_STRENGTH, BASE_DEXTERITY, BASE_INTELLIGENCE);

    }

    public void levelUp() {
        super.levelUp(LEVEL_STRENGTH, LEVEL_DEXTERITY, LEVEL_INTELLIGENCE);
    }

    /**
     * Updates the total attributes for the character, adding in armor attributes.
     * @param equipment the rogues equipment
     */
    public void updateTotalAttributes(RogueEquipment equipment) {
        super.updateTotalAttributes(equipment);
    }

    /**
     * Calculates the rogues dps.
     */
    public void calculateDPS(RogueEquipment equipment) {
        super.calculateDPS(this.getTotal().getDexterity(), equipment);
    }

}
