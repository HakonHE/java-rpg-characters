package no.noroff.accelerate;

import no.noroff.accelerate.character.*;
import no.noroff.accelerate.character.Character;
import no.noroff.accelerate.equipment.*;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        int selector;
        System.out.print("What do you want your player named? ");
        Scanner myObj = new Scanner(System.in);
        String charName = myObj.nextLine();

        String classSelector = """

                Which class do you want to play?
                Warrior
                Mage
                Ranger
                Rogue
                Enter classname:\s""";
        System.out.print(classSelector);
        String className = myObj.nextLine();

        switch (className) {
            case "Warrior" -> {
                Warrior player = new Warrior(charName);
                WarriorEquipment equipment = new WarriorEquipment(player);
                player.updateTotalAttributes(equipment);
                player.calculateDPS(equipment);
                do {
                    selector = warriorMenu(player);
                } while ( selector != 0 );
            }
            case "Mage" -> {
                Mage player = new Mage(charName);
                MageEquipment equipment = new MageEquipment(player);
                player.updateTotalAttributes(equipment);
                player.calculateDPS(equipment);
                do {
                    selector = mageMenu(player);
                } while ( selector != 0 );
            }
            case "Ranger" -> {
                Ranger player = new Ranger(charName);
                RangerEquipment equipment = new RangerEquipment(player);
                player.updateTotalAttributes(equipment);
                player.calculateDPS(equipment);
                do {
                    selector = rangerMenu(player);
                } while ( selector != 0 );
            }
            case "Rogue" -> {
                Rogue player = new Rogue(charName);
                RogueEquipment equipment = new RogueEquipment(player);
                player.updateTotalAttributes(equipment);
                player.calculateDPS(equipment);
                do {
                    selector = rogueMenu(player);
                } while ( selector != 0 );
            }
            default -> System.out.println("\nNo class with that name. Exiting.");
        }
    }

    public static int mageMenu(Mage player) {
        int selector;
        String menu = """

                Menu:
                \t1: Show Character Sheet
                \t2: Level Up Character
                \t0: Exit
                Choose what you want to do(1,2):\s""";
        System.out.print(menu);
        Scanner myInt = new Scanner(System.in);
        selector = myInt.nextInt();
        switch (selector) {
            case 1 -> displayCharacterSheet(player);
            case 2 -> {
                player.levelUp();
                System.out.println("\nCongratulations you leveled up! You are now level "
                        + player.getLevel() + "!\n");
            }
            default -> selector = 0;
        }
        return selector;
    }

    public static int warriorMenu(Warrior player) {
        int selector;
        String menu = """

                Menu:
                \t1: Show Character Sheet
                \t2: Level Up Character
                \t0: Exit
                Choose what you want to do(1,2):\s""";
        System.out.print(menu);
        Scanner myInt = new Scanner(System.in);
        selector = myInt.nextInt();
        switch (selector) {
            case 1 -> displayCharacterSheet(player);
            case 2 -> {
                player.levelUp();
                System.out.println("\nCongratulations you leveled up! You are now level "
                        + player.getLevel() + "!\n");
            }
            default -> selector = 0;
        }
        return selector;
    }

    public static int rangerMenu(Ranger player) {
        int selector;
        String menu = """

                Menu:
                \t1: Show Character Sheet
                \t2: Level Up Character
                \t0: Exit
                Choose what you want to do(1,2):\s""";
        System.out.print(menu);
        Scanner myInt = new Scanner(System.in);
        selector = myInt.nextInt();
        switch (selector) {
            case 1 -> displayCharacterSheet(player);
            case 2 -> {
                player.levelUp();
                System.out.println("\nCongratulations you leveled up! You are now level "
                        + player.getLevel() + "!\n");
            }
            default -> selector = 0;
        }
        return selector;
    }

    public static int rogueMenu(Rogue player) {
        int selector;
        String menu = """

                Menu:
                \t1: Show Character Sheet
                \t2: Level Up Character
                \t0: Exit
                Choose what you want to do(1,2):\s""";
        System.out.print(menu);
        Scanner myInt = new Scanner(System.in);
        selector = myInt.nextInt();
        switch (selector) {
            case 1 -> displayCharacterSheet(player);
            case 2 -> {
                player.levelUp();
                System.out.println("\nCongratulations you leveled up! You are now level "
                        + player.getLevel() + "!\n");
            }
            default -> selector = 0;
        }
        return selector;
    }

    public static void displayCharacterSheet(Character player) {
        StringBuilder charSheet = new StringBuilder("\nCharacter Sheet:");
        charSheet.append("\n");
        charSheet.append("Name: ").append(player.getCharacterName()).append("\n");
        charSheet.append("Level: ").append(player.getLevel()).append("\n");
        charSheet.append("Stats: ").append("\n");
        charSheet.append(" Strength: ").append(player.getTotal().getStrength()).append("\n");
        charSheet.append(" Dexterity: ").append(player.getTotal().getDexterity()).append("\n");
        charSheet.append(" Intelligence: ").append(player.getTotal().getIntelligence()).append("\n");
        charSheet.append(" DPS: ").append(player.getCharDPS()).append("\n");

        System.out.println(charSheet);
    }
}
