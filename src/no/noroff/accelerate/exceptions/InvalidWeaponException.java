package no.noroff.accelerate.exceptions;

/**
 * An exception class for invalid weapon.
 */
public class InvalidWeaponException extends Exception{
    public InvalidWeaponException (String message)
    {
        super(message);
    }
}
