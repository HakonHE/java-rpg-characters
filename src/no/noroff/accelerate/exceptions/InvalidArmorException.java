package no.noroff.accelerate.exceptions;

/**
 * An exception class for invalid armor.
 */
public class InvalidArmorException extends Exception {
    public InvalidArmorException (String message)
    {
        super(message);
    }
}
