package no.noroff.accelerate.item;

public enum ArmorType {
    CLOTH,
    LEATHER,
    MAIL,
    PLATE
}
