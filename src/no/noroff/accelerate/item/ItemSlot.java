package no.noroff.accelerate.item;

public enum ItemSlot {
    WEAPON,
    HEAD,
    BODY,
    LEGS
}
