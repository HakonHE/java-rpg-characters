package no.noroff.accelerate.item;

/**
 * Abstract class Item
 */
public abstract class Item{
   private final String name;
   private final int requiredLevel;
   private ItemSlot slot;

   public Item(String itemName, int reqLevel) {
      name = itemName;
      requiredLevel = reqLevel;
   }

   public int getRequiredLevel() {
      return requiredLevel;
   }

   public ItemSlot getSlot() {
      return slot;
   }

   public void setItemSlot(ItemSlot slot) {
      this.slot = slot;
   }

   public String getName() {
      return name;
   }
}
