package no.noroff.accelerate.item;

import no.noroff.accelerate.PrimaryAttribute;

/**
 * An armor class which is a child component of the Item class.
 */
public class Armor extends Item{
    PrimaryAttribute armorAttributes;
    ArmorType armorType;

    public PrimaryAttribute getArmorAttributes() {
        return armorAttributes;
    }

    public void setArmorAttributes(PrimaryAttribute armorAttributes) {
        this.armorAttributes = armorAttributes;
    }

    public ArmorType getArmorType() {
        return armorType;
    }

    public void setArmorType(ArmorType armorType) {
        this.armorType = armorType;
    }

    public Armor(String name, int requiredLevel) {
        super(name, requiredLevel);
        armorAttributes = new PrimaryAttribute(0, 0, 0);
    }

}
