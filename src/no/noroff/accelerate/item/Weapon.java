package no.noroff.accelerate.item;

/**
 * A weapon class which is a child component of Item.
 */
public class Weapon extends Item{
    private int damage = 1;
    private double attackSpeed = 1;
    private final WeaponType type;

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public void setAttackSpeed(double attackSpeed) {
        this.attackSpeed = attackSpeed;
    }

    /**
     * Function that calculates dps then returns it to function call.
     * @return double with weapon dps
     */
    public double getDPS() {
        return damage * attackSpeed;
    }

    public WeaponType getType() {
        return type;
    }

    public Weapon(String name, int reqLevel, WeaponType type) {
        super(name, reqLevel);
        this.type = type;
    }
}
