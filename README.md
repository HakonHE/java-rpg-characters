# RPG Characters

[![standard-readme compliant](https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

A console application to create an RPG character, check its stats and level it up.

## Table of Contents

- [Background](#background)
- [Install](#install)
- [Usage](#usage)
- [Maintainers](#maintainers)
- [Contributing](#contributing)

## Background

This project is the first assignment of the Java Full Stack course. 

The task was to create a console application. With the features:
* Various character classes having attributes which increase at different rates as the character gains levels.
* Equipment, such as armor and weapons, that characters can equip. The equipped items will alter the power of the character, causing it to deal more damage and be able to survive longer. Certain characters can equip certain item types.
* Custom exceptions. There are two custom exceptions required to write.
* Full test coverage of the functionality. Some testing data is provided, it can be used to complete the assignment in a test-driven development manner.


## Install

* Install JDK 17
* Install Intellij
* Clone repository

## Usage

* Run the application
* Test the functionalities

## Maintainers

[@HåkonHE](https://gitlab.com/HåkonHE)

## Contributing

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

